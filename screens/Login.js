import React, { Component } from 'react';
import {
	View,
	Button,
	Text,
	StyleSheet,
	TextInput,
	TouchableOpacity,
	StatusBar,
	Image,
	Alert,
	Linking,
	ImageBackground,
	Platform,
	Modal,
}
from 'react-native';

import { KeyboardAvoidingView } from 'react-native';
import axios from 'axios';

export class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			login: ''
		}
	}

	doLogin = () => {
		var data = {
			op: "login",
			login: this.state.login,
			password: this.state.senha
		};
		console.log(data);
		axios.post('https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/check-point', data)
			.then(result => {
				console.log(JSON.stringify(result.data));
				if (result.data.length > 0) {
					this.props.navigation.navigate('SelectType');
				}
				else {
					Alert.alert('Usuario ou senha invalidos');
				}
			}).catch(error => {
				Alert.alert('Usuario ou senha invalidos');
			});

	}


	render() {
		return (

			<ImageBackground style={{ tintColor:'#1D2C4D', flex: 1, resizeMode: 'cover', backgroundColor: 'transparent', justifyContent: 'center' }}>
				<View style={styles.overlay}>
				<KeyboardAvoidingView behavior="padding" enabled>
			        <Image
			          source={require('../assets/images/pontos_viciados_icon.png')}
			          style={{
			        	width: 260,
			        	height: 140,
			            alignSelf: 'center',
			            resizeMode: 'stretch',
			          }} />
			          
			        <View style={{textAlign: 'center', marginTop:10,  marginBottom:40, marginLeft: 80, marginRight: 80}}>
	                	<Text style={{	textAlign: 'center', fontSize:14, color:'white', align: 'center'}}>
	                    	PROGRAMA DE INCENTIVO AO
	                    	<Text style={{marginLeft: 60, color: '#FE574F', fontWeight:'bold'}}> COMBATE </Text> 
	                    	DE DESCARTE IRREGULAR DE
	                    	<Text style={{color: '#FE574F', fontWeight:'bold'}}> ENTULHO</Text> 
	                    </Text>
	                </View>
			
			        <TextInput
			          value={this.state.login}
			          onChangeText={login => this.setState({ login })}
			          keyboardType='email-address'
			          underlineColorAndroid='transparent'
			          placeholder='CPF'
			          placeholderTextColor='gray'
			          style={{
			        	paddingHorizontal: 5,
			        	borderColor:'#159980',
			            backgroundColor: 'white',
			            borderRadius: 5,
			            marginHorizontal: 40,
			            marginBottom: 20,
			            fontSize: 18,
			            height: 35,
			            color: 'black',
			          }} />
			
			        <TextInput
			          value={this.state.senha}
			          onChangeText={senha => this.setState({ senha })}
			          underlineColorAndroid='transparent'
			          placeholder='Senha'
			          secureTextEntry={true}
			          placeholderTextColor='gray'
			          style={{
			        	paddingHorizontal: 5,
			            backgroundColor: 'white',
			            borderRadius: 5,
			            borderColor:'#159980',
			            marginHorizontal: 40,
			            marginBottom: 20,
			            fontSize: 18,
			            height: 35,
			            color: 'black',
			          }} />
				
			        <TouchableOpacity
			          disabled={this.state.refresh}
			          onPress={() => this.doLogin()} style={{
			            height: 40,
			            paddingHorizontal: 10,
			            justifyContent: 'center',
			            marginHorizontal: 40,
			            marginBottom: 35,
			            borderRadius: 5,
			            color: 'white',
			            backgroundColor: '#159980'
			          }}>
			          <Text style={{
			            color: 'white',
			            fontSize: 16,
			            textAlign: 'center'
			          }}>Confirmar</Text>
			        </TouchableOpacity>
				
				</KeyboardAvoidingView>
				
		        
				</View>
		      </ImageBackground>

		);

	}

}

const mapStateToProps = (state) => {
	return {
		status: state.auth.status
	};
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	header: {
		margin: 20
	},
	body: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	bg: {
		width: 332,
		height: 255
	},
	button: {
		width: '90%',
		padding: 17,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#FA807C',
		borderRadius: 10
	},
	buttonText: {
		fontWeight: 'bold',
		fontSize: 15,
		color: '#FFF'
	},
	heading_1: {
		fontWeight: 'bold',
		color: '#4a4f53',
		alignSelf: 'flex-start',
		marginTop: 20
	},
	heading_2: {
		color: '#ff3c36',
		fontWeight: 'bold',
		fontSize: 30
	},
	heading_3: {
		fontWeight: 'bold',
		color: '#a8a9ab',
		textAlign: 'center'
	},
	heading_4: {
		fontWeight: 'bold',
		color: '#4a4f53'
	},
	heading_5: {
		fontWeight: 'bold',
		color: '#ff3c36',
		textAlign: 'center',
		marginBottom: 20
	},
	line: {
		borderWidth: 3,
		borderColor: '#a9acb1',
		marginBottom: 5,
		borderRadius: 5
	},
	footer: {

	},
	overlay: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#1D2C4D',
	},
});

export default Login;
